import requests
import json

wekbook_url = 'https://hooks.slack.com/services/{...TOKEN...}}'

data = {
        'text': 'Orchestration {XYX} finished! \n',
        'username': 'KBC Automation',
        'icon_emoji': ':keboola:'
    }

response = requests.post(wekbook_url, data=json.dumps(data), headers={'Content-Type': 'application/json'})

print('Response: ' + str(response.text))
print('Response code: ' + str(response.status_code))