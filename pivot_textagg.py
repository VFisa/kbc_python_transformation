import pandas as pd
"""
pivoting strings
"""

model = pd.read_csv("/data/in/tables/facebook_actions.csv",low_memory=False)
print(model.columns)

pivot_table = model.pivot_table(index='JSON_parentId',
                                     columns='action_type', 
                                     values='value',
                                     aggfunc=lambda x: ' '.join(str(v) for v in x))

pivot_table.to_csv("/data/out/tables/data_pivoted.csv")
